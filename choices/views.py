from django.shortcuts import render
from rest_framework import generics

from .models import Todo
from .serializers import TodoSerializer


def index(request):
    return render(request, 'choices/page.html')


class TodoListCreateAPIView(generics.ListCreateAPIView):
    """ Список Выполнить
    """
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
