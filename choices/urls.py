from django.urls import path
from . import views


urlpatterns = [
    path('', views.index),
    path('api/todos/', views.TodoListCreateAPIView.as_view(), name='Todo')
]
