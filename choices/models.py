from django.db import models

class Todo(models.Model):
    STATUSES = (
        ('A', 'Active'),
        ('C', 'Complete'),
        ('E', 'Expired'),
        ('D', 'Deferred'),
    )
    deadline = models.DateTimeField(blank=True)
    action = models.CharField(max_length=255)
    status = models.CharField(max_length=1, choices=STATUSES)

    def __str__(self):
        return f"{self.deadline}: {self.action} ({self.status}) "
